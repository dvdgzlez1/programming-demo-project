﻿namespace Demo.Models
{
    public class ActionModelResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}