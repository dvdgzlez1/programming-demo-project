﻿namespace Demo.Models
{
    public class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int SalesRank { get; set; }
        public string ASIN { get; set; }
        //Aditional Data
        public string Category { get; set; }
    }
}