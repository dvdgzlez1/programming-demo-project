﻿namespace Demo.Models
{
    public class LuminatiModel
    {
        public string ip { get; set; }
        public int port { get; set; }
        public Credentials credential { get; set; }
    }

    public class Credentials
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}