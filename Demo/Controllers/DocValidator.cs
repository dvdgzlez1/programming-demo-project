﻿using HtmlAgilityPack;

namespace Demo.Controllers
{
    public class DocValidator
    {
        public static bool IsValid(HtmlDocument doc)
        {
            if (doc == null)
            {
                return false;
            }
            var title = doc.DocumentNode.SelectSingleNode("/html/head/title");
            if (title == null)
            {
                return false;
            }
            if (title.InnerText.Contains("Sorry") ||
                title.InnerText.Contains("Robot Check"))
            {
                return false;
            }
            return true;
        }
    }
}