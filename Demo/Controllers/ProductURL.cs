﻿using System;
using System.Linq;
using Demo.Models;
using HtmlAgilityPack;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;

namespace Demo.Controllers
{
    public class ProductURL
    {
        private Product product = new Product();
        private string _url;

        public ProductURL()
        {

        }

        public ProductURL(string url)
        {
            _url = url;
        }

        public Product GetProduct()
        {
            try
            {
                FormatProductURL(_url);
            }
            catch (Exception e)
            {
                //Log.Trace("GetProduct in ProductURL " + e.Message);
                //move on
            }

            return product;
        }

        private void FormatProductURL(string url)
        {
            try
            {
                int count = 0;
                do
                {
                    Thread.Sleep(1);
                    var doc = Connection.Load(url);
                    if (DocValidator.IsValid(doc))
                    {
                        var nameNode = doc.DocumentNode.SelectSingleNode("//*[@id=\"productTitle\"]");
                        var priceNode = doc.DocumentNode.SelectSingleNode("//*[@id=\"olp_feature_div\"]/div/span/a");
                        var additionalInfoHeadNode = doc.DocumentNode.SelectNodes("//*[@id=\"productDetails_detailBullets_sections1\"]//tr//th");
                        var additionalInfoDataNode = doc.DocumentNode.SelectNodes("//*[@id=\"productDetails_detailBullets_sections1\"]//tr//td");

                        if (nameNode == null || priceNode == null || additionalInfoDataNode == null || additionalInfoHeadNode == null)
                        {
                            throw new ArgumentException("Product values are not valid");
                        }

                        product.Name = nameNode.InnerText.Trim();
                        product.Price = GetPrice(priceNode);
                        product.ASIN = GetASIN(additionalInfoHeadNode, additionalInfoDataNode);
                        product.SalesRank = GetSalesRank(additionalInfoHeadNode, additionalInfoDataNode);
                        Log.Trace("Thread: " + Thread.CurrentThread.ManagedThreadId + " || url: " + url + " || " + JsonConvert.SerializeObject(product));
                        break;
                    }
                    else
                    {
                        count++;
                        continue;
                    }
                } while (count < 350);
            }
            catch (Exception e)
            {
                //Log.Trace("Exception in FormatProductURL " + e.Message);
                //Log.Trace("ProductURL: This product couldn't be processed");
                throw new ArgumentException();
            }
        }

        private double GetPrice(HtmlNode node)
        {
            var priceHTML = node.InnerText.Trim().Split(' ');
            double price = double.Parse(Array.Find(priceHTML, (n) => n.Contains('$')).Replace("$", "").Replace('.', ','));
            if (!ValidPrice(price))
            {
                throw new ApplicationException("The price is not valid for the application");
            }
            return price;
        }

        private string GetASIN(HtmlNodeCollection headNodes, HtmlNodeCollection dataNodes)
        {
            foreach (var headNode in headNodes)
            {
                if (headNode.InnerHtml.Trim().Equals("ASIN"))
                {
                    return dataNodes[headNodes.IndexOf(headNode)].InnerText.Trim();
                }
            }
            throw new ArgumentException("Couldn't find any ASIN value");
        }

        private int GetSalesRank(HtmlNodeCollection headNodes, HtmlNodeCollection dataNodes)
        {
            foreach (var headNode in headNodes)
            {
                if (headNode.InnerHtml.Trim().Equals("Best Sellers Rank"))
                {
                    var salesRankHTML = dataNodes[headNodes.IndexOf(headNode)].InnerText.Trim().ToLower().Split(' ');
                    string salesString = Array.Find(salesRankHTML, (n) => n.Contains('#')).Replace("#", "").Replace(",", "");
                    int salesRank = int.Parse(salesString);
                    if (!ValidSalesRank(dataNodes[headNodes.IndexOf(headNode)].InnerText.Trim().ToLower(), salesRank))
                    {
                        throw new ApplicationException("The salesRank is not valid for the application");
                    }
                    return salesRank;
                }
            }
            throw new ArgumentException("Couldn't find any salesRank value");
        }

        private bool ValidSalesRank(string salesRankHTML, int salesRank)
        {
            return salesRankHTML.Contains("see top 100") && salesRank < 100000;
        }

        private bool ValidPrice(double price)
        {
            return price > 150;
        }

    }
}