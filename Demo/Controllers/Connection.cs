﻿using System;
using System.Net;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Security.Authentication;

namespace Demo.Controllers
{
    public static class Connection
    {
        private static Random r = new Random();
        private static HtmlWeb web = new HtmlWeb();
        private static Models.LuminatiModel host = JsonConvert.DeserializeObject<Models.LuminatiModel>(File.ReadAllText(@"C:\JsonData\luminati.json"));
        private static List<string> userAgents = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(@"C:\JsonData\userAgent.json"));

        public static HtmlDocument Load(string url)
        {
            try
            {
                WebProxy proxy = new WebProxy(host.ip + ":" + host.port.ToString());
                var netCredentials = new NetworkCredential(host.credential.username, host.credential.password);
                proxy.Credentials = netCredentials;

                const SslProtocols _Tls12 = (SslProtocols)0x00000C00;
                const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
                ServicePointManager.SecurityProtocol = Tls12;
                HtmlWeb web = new HtmlWeb()
                {
                    PreRequest = request =>
                    {
                        // Make any changes to the request object that will be used.
                        request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                        return true;
                    }
                };

                web.UserAgent = userAgents[r.Next(userAgents.Count - 1)];
                return web.Load(url, "GET", proxy, netCredentials);
            }
            catch (OutOfMemoryException o)
            {
                Thread.Sleep(10);
                return null;
            }
            catch (Exception e)
            {
                Log.Trace("Connection Load Exception " + e.Message + " | " + e.StackTrace);
                return null;
            }
        }
    }

    class Proxy
    {
        public string proxy { get; set; }
    }
}