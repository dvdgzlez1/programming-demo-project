﻿using Demo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.Controllers
{
    public class Action
    {
        public static async Task<string> Init(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return JsonConvert.SerializeObject(new ActionModelResult { Success = false, Message = "The URL is not provided" });
            }

            List<string> productsASIN;

            try
            {
                Searcher s = new Searcher(url);
                productsASIN = await s.ProcessASIN();
                if (productsASIN.Count == 0)
                {
                    return JsonConvert.SerializeObject(new ActionModelResult { Success = false, Message = "Couldn't get any products from url" });
                }
            }
            catch (ArgumentException a)
            {
                return JsonConvert.SerializeObject(new ActionModelResult { Success = false, Message = a.Message });
            }
            catch
            {
                //Log.Trace(e.Message);
                return JsonConvert.SerializeObject(new ActionModelResult { Success = false, Message = "A fatal error ocurred :(" });
            }

            List<Product> products = Worker.Scrape(Formatter.ProductsURL(productsASIN));

            if (products.Count == 0)
            {
                return JsonConvert.SerializeObject(new ActionModelResult { Success = false, Message = "Finished without any products" });
            }

            Log.Trace("All done =)");
            return JsonConvert.SerializeObject(new ActionModelResult { Success = true, Message = "You can download the file :)" });
        }
    }
}