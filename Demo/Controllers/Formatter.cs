﻿using System.Collections.Generic;
using System.Linq;
using Demo.Models;
using System.IO;
using Newtonsoft.Json;

namespace Demo.Controllers
{
    public class Formatter
    {
        public static List<string> ProductsURL(List<string> asins)
        {
            return asins.Select(asin => "https://www.amazon.com/dp/" + asin).ToList();
        }

        public static List<Product> GetResultProducts()
        {
            try
            {
                return JsonConvert.DeserializeObject<List<Product>>(File.ReadAllText(@"C:\JsonData\result.json"));
            }
            catch
            {
                return new List<Product>();
            }
        }
    }
}