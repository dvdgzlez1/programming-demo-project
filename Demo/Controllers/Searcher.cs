﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System.Threading;

namespace Demo.Controllers
{
    public class Searcher
    {
        private HtmlWeb web = new HtmlWeb();
        private string _seller;
        private string _url;
        
        public Searcher() { }

        public Searcher(string url)
        {
            _url = url;
        }

        public async Task<List<string>> ProcessASIN()
        {
            if (!Valid(_url))
            {
                throw new ArgumentException("The url is not valid");
            }

            // Get the seller/merchant value to search their asins
            _seller = HttpUtility.ParseQueryString(new Uri(_url).Query)["seller"];

            if (string.IsNullOrEmpty(_seller)) {
                var merchant = HttpUtility.ParseQueryString(new Uri(_url).Query)["merchant"];
                if (string.IsNullOrEmpty(merchant))
                {
                    throw new ArgumentException("The url is not valid");
                }
                _seller = merchant;
            }
                

            Log.Trace("Seller: " + _seller);

            return await GetAllASINs();
        }

        private async Task<List<string>> GetAllASINs()
        {
            int pageNum = 1;
            List<string> Total = new List<string>();
            var task = await Task.Factory.StartNew(() => PartialASINs(pageNum++));

            if (task.code == -2 || task.code == -1)
            {
                return Total;
            }

            Total.AddRange(task.ASINs);

            List<CodeList> taskList = new List<CodeList>();
            List<Thread> threads = new List<Thread>();
            for (int i = 2; i <= task.code; i++)
            {
                int temp = i;
                Thread th = new Thread(new ThreadStart(() =>
                {
                    taskList.Add(PartialASINs(temp));
                }));
                th.IsBackground = true;
                threads.Add(th);
            }

            try
            {
                threads.ForEach((th) => th.Start());
            }
            catch (ThreadStateException e)
            {

            }

            bool waitAll = true;

            while (waitAll)
            {
                waitAll = false;
                Thread.Sleep(1);
                foreach (var th in threads)
                {
                    if (th.IsAlive)
                    {
                        waitAll = true;
                    }
                }
            }

            var allASINs = taskList.Distinct().ToList()
                                   .Where((code) => code.ASINs != null)
                                   .Select((code) => code.ASINs);

            foreach (var asin in allASINs)
            {
                Total.AddRange(asin);
            }

            return Total;
        }

        private CodeList PartialASINs(int pageNum)
        {
            int count = 0, page = 0;
            string url = "https://www.amazon.com/s?merchant=" + _seller + "&page=" + pageNum.ToString();
            Log.Trace(url);
            try
            {
                do
                {
                    Thread.Sleep(1);
                    var doc = Connection.Load(url);

                    if (DocValidator.IsValid(doc))
                    {
                        var nameNode = doc.DocumentNode.SelectNodes("//*[@data-asin]");
                        if (nameNode == null)
                        {
                            break;
                        }

                        var values = nameNode.Select(node => node.Attributes["data-asin"].Value);
                        if (pageNum == 1)
                        {
                            var pageNode = doc.DocumentNode.SelectSingleNode("//*[contains(@class, 'pagnDisabled')]");
                            if (pageNode == null)
                            {
                                pageNode = doc.DocumentNode.SelectSingleNode("//*[contains(@class, 'pagnLink')]");
                            }
                            page = int.Parse(pageNode == null ? "1" : pageNode.InnerText);
                            Log.Trace("Total pages " + page);
                        }
                        //Log.Trace("Values received " + JsonConvert.SerializeObject(values.ToList()));
                        return new CodeList() { code = page, ASINs = values.ToList() };
                    }
                    else
                    {
                        count++;
                        continue;
                    }
                } while (count < 350);
                Log.Trace("Couldn't do it =/");
                return new CodeList() { code = -1 };
            }
            catch (Exception e)
            {
                Log.Trace("PartialASINs error " + e.Message + " | " + e.StackTrace);
                return new CodeList() { code = -2 };
            }
        }
                

        private bool Valid(string url)
        {
            Uri uriResult;
            return Uri.TryCreate(url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }
    }
    
    class CodeList
    {
        public List<string> ASINs { get; set; }
        public int code { get; set; }
    }
}