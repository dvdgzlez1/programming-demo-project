﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Models;
using Newtonsoft.Json;
using System.Threading;

namespace Demo.Controllers
{
    public class Worker
    {
        public static List<Product> Scrape(List<string> urls)
        {
            List<Product> products = new List<Product>();
            List<Thread> threads = new List<Thread>();
            Log.Trace(System.DateTime.Now.ToShortTimeString());
            //Log.Trace(JsonConvert.SerializeObject(urls));
            Log.Trace("Scraping " + urls.Count.ToString() + " product's URLs");
            foreach (var url in urls)
            {
                string temp = url;
                try
                {
                    threads.Add(new Thread(new ThreadStart(() =>
                    {
                        products.Add(new ProductURL(temp).GetProduct());
                    })));
                }
                catch
                {
                    //move on
                }
            }

            try
            {
                threads.ForEach((th) => th.Start());
            }
            catch (ThreadStateException te)
            {
                Log.Error(te.ToString());
            }


            bool waitAll = true;

            while (waitAll)
            {
                waitAll = false;
                Thread.Sleep(10);
                foreach (var th in threads)
                {
                    if (th.IsAlive)
                    {
                        waitAll = true;
                    }
                }
            }

            List<Product> final = products.Distinct().ToList()
                                          .Where(product => product.SalesRank != 0)
                                          .Select(product => product).ToList();
            final.ForEach(product => { product.Price *= 0.95; });
            Log.SaveProducts(final);
            return final;
        }
    }
}