﻿using System.Collections.Generic;
using Demo.Models;
using System.IO;
using Newtonsoft.Json;

namespace Demo.Controllers
{
    public static class Log
    {
        public static void SaveProducts(List<Product> products)
        {
            File.WriteAllText(@"C:\JsonData\result.json", JsonConvert.SerializeObject(products));
        }

        public static void Trace(string data)
        {
            string path = @"C:\JsonData\log.txt";
            try
            {
                using (var file = new StreamWriter(path, true))
                {
                    file.WriteLine("---");
                    file.WriteLine(data);
                    file.WriteLine("---");
                }
            }
            catch
            {

            }
        }

        public static void Error(string data)
        {
            string path = @"C:\JsonData\error.txt";
            try
            {
                using (var file = new StreamWriter(path, true))
                {
                    file.WriteLine("---");
                    file.WriteLine(data);
                    file.WriteLine("---");
                }
            }
            catch
            {

            }
        }
    }
}