﻿using System.Threading.Tasks;
using System.Web.Mvc;
using ClosedXML.Excel;
using System.Web;
using System.IO;

namespace Demo.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        public ActionResult Index()
        {
            return View();
        }

        public async Task<string> StartAction(string url)
        {
            return await Action.Init(url);
        }

        [HttpGet]
        public virtual ActionResult GetResult()
        {
            XLWorkbook workbook = new XLWorkbook(@"C:\JsonData\Inventory.xlsx");
            var worksheet = workbook.Worksheet("InventoryLoaderTemplate");
            var Products = Formatter.GetResultProducts();

            int row = 2;
            foreach (var product in Products)
            {
                worksheet.Cell(row, 1).SetValue("ASIN" + product.ASIN);
                worksheet.Cell(row, 2).SetValue(product.ASIN);
                worksheet.Cell(row, 3).SetValue(1);
                worksheet.Cell(row, 4).SetValue(product.Price);
                row++;
            }

            workbook.Worksheet("InventoryLoaderTemplate").SetTabActive(true);

            // Prepare the response
            HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Result.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
            }

            httpResponse.End();
            return View();
        }
    }
}