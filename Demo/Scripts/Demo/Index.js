﻿(function () {
    var downloadDiv = $(".download"),
        infoDiv = $(".info"),
        spinner = $("#spinner"),
        url = window.location.href;

    $(document).ready(function () {
        $("#competitorURL").attr("placeholder", "Competitors link here...");

        // the following is to handle cases where the times are on the opposite side of
        // midnight e.g. when you want to get the difference between 9:00 PM and 5:00 AM

        $("#start_button").click(() => {
            idleState()
            var startDate = Date.now();
            $.post(urlFormatted("StartAction"), { url: $("#competitorURL").val() })
                .done((value) => {
                    var endDate = Date.now();
                    if (endDate < startDate) {
                        endDate.setDate(startDate.getDate() + 1);
                    }

                    var diff = (endDate - startDate) / 1000;
                    var data = JSON.parse(value);
                    $("#info_message").text(data.Message);
                    if (data.Success) {
                        $("#info_message").append(" " + diff + " seconds");
                        successState();
                    }
                    else {
                        failedState();
                    }
                });
        });
        $("#download").click(() => {
            window.location = urlFormatted("GetResult");
        });
    });

    function idleState() {
        $("#info_message").text("Working...");
        infoDiv.removeClass("failed").removeClass("success").addClass("idle");
        spinner.removeClass("invisible").addClass("visible");
        downloadDiv.removeClass("visible").addClass("invisible");
    }

    function successState() {
        infoDiv.removeClass("failed").removeClass("idle").addClass("success");
        spinner.removeClass("visible").addClass("invisible");
        downloadDiv.removeClass("invisible").addClass("visible");
    }

    function failedState() {
        infoDiv.removeClass("success").removeClass("idle").addClass("failed");
        spinner.removeClass("visible").addClass("invisible");
        downloadDiv.removeClass("visible").addClass("invisible");
    }

    function urlFormatted(action) {
        if (url.charAt(url.length - 1) !== '/')
        {
            url += '/';
        }
        return url.includes("Demo") ? url + action : url + 'Demo/' + action;
    }
})();